<?php

require_once('models/Product.php');

class ProductController
{

    function __construct()
    {
        echo "constructor ProductController<br>";
    }

    public function index()
    {
        $products = Product::all();
        require('views/product/index.php');
    }

    public function create()
    {
        require('views/product/create.php');
    }

    public function store()
    {
        $product = new Product;
        $product->id = $_POST['id'];
        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];
        $product->fecha = DateTime::createFromFormat('Y-m-d', $_POST['fecha']);
        $product->tipo = $_POST['id_tipo'];

        $product->store();
        header('location:index');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        include 'views/product/edit.php';
    }

    public function update($id)
    {
        $product = Product::find($id);

        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];
        $product->fecha = $_POST['fecha'];
        $product->tipo = $_POST['id_tipo'];

        $product->save();

        header('location:../index');
    }

    public function delete($id)
    {
        $product = Product::find($id);
        $product->delete();
        header('location:../index');
    }
    #<br /><b>Notice</b>:  Undefined property: Product::$tipo in <b>/var/www/examen1701/views/product/edit.php</b> on line <b>20</b><br />
}
