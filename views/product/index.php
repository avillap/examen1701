<?php require 'views/header.php'; ?>
    <main>
        <h1>Lista de productos</h1>

        <table>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Fecha</th>
                <th>Tipo</th>
                <th>Acciones</th>
            </tr>
            <?php foreach ($products as $product): ?>
            <tr>
            <td><?php echo $product->id ?></td>
            <td><?php echo $product->nombre ?></td>
            <td><?php echo $product->precio ?></td>
            <td><?php echo $product->fecha ?></td>
            <td><?php echo $product->id_tipo ?></td>
            <td>
                <a href="<?php echo "visit/$product->id" ?>">Visitar</a>
                 -
                <a href="<?php echo "edit/$product->id" ?>">editar</a>
            </td>
            </tr>
            <?php endforeach ?>
        </table>

        <a href="product/create">Nuevo</a>
  </main>
<?php require 'views/footer.php'; ?>


