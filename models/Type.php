<?php
/**
*
*/
class Book
{
    public $id;
    public $nombre;

    function __construct()
    {

    }

    public function connect()
    {
        $dsn = 'mysql:dbname=examen1701;host=127.0.0.1';
        $usuario = 'root';
        $contraseña = 'root';

        try {
            $db = new PDO($dsn, $usuario, $contraseña, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // die('Conectado');
        } catch (PDOException $e) {
            die ('Falló la conexión: ' . $e->getMessage());
        }
        return $db;
    }

    public static function all()
    {
        $db = Type::connect();

        $stmt = $db->query('SELECT * FROM tipo');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Type');
        $results = $stmt->fetchAll();
        return $results;
    }

    public static function find($id)
    {
        $db = User::connect();

        $stmt = $db->query('SELECT * FROM tipo WHERE id=' . $id);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
        $result = $stmt->fetch();
        return $result;
    }

    public function save()
    {
        $db = $this->connect();
        $sql = "update tipo set nombre=:nnombre where id=:id";
        $query = $db->prepare($sql);
        $query->bindParam(":nombre", $this->nombre);
        $query->bindParam(":id", $this->id);
        $result = $query->execute();
        // return $result;
    }


    public function store()
    {
        $db = $this->connect();

        $sql = "INSERT INTO tipo(id, nombre) VALUES(?, ?)";

        $query = $db->prepare($sql);
        $query->bindParam(1, $this->id);
        $query->bindParam(2, $this->nombre);

        return $query->execute();
    }
}
