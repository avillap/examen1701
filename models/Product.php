<?php

/**
*
*/
require('app/Model.php');

class Product extends Model
{
    public $id;

    function __construct()
    {
        # code...
    }


    public static function all()
    {
        $db = Product::connect();

        $stmt = $db->prepare("SELECT * FROM producto");
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Product');

        $results = $stmt->fetchAll();
        return $results;
    }

    //hace el INSERT
    public function store()
    {
        $db = $this->connect();
        $sql = "INSERT INTO producto(id, nombre, precio, fecha, id_tipo) VALUES(?, ?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1, $this->id);
        $stmt->bindParam(2, $this->nombre);
        $stmt->bindParam(3, $this->precio);
        $stmt->bindParam(4, $this->fecha->format('Y-m-d'));
        $stmt->bindParam(5, $this->id_tipo);

        $result = $stmt->execute();
        return $result;
    }

    public function find($id)
    {
        $db = Product::connect();
        $sql = "SELECT * FROM producto WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Product');
        $result = $stmt->fetch();
        return $result;
    }

    public function save()
    {
        $db = $this->connect();
        $sql = "UPDATE producto SET id=?, nombre=?, precio=?, fecha=?, id_tipo=? WHERE id=?";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1, $this->id);
        $stmt->bindParam(2, $this->nombre);
        $stmt->bindParam(3, $this->precio);
        $stmt->bindParam(4, $this->fecha);
        $stmt->bindParam(5, $this->id_tipo);

        $result = $stmt->execute();
        return $result;
    }

    public function delete()
    {
        $db = $this->connect();
        $sql = "DELETE FROM producto WHERE id=?";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1, $this->id);
        $result = $stmt->execute();
        return $result;
    }
}
